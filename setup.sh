#!/usr/bin/env bash

##
##  Note:
##      Script won't run properly on OSX. The OpenBSD version of "tar" doesn't support target folder parameter and "strip-components" directive.
##      In that case, unpack "wordpress-4.9.6.tar.gz" manually and move it to /blog folder
##
##

cp .env.example app/config/env.php

mkdir blog
wget https://wordpress.org/wordpress-4.9.6.tar.gz
tar -xzvf wordpress-4.9.6.tar.gz -C blog --strip-components=1

ln -s "$PWD/app/wp-theme" blog/wp-content/themes/putinwp


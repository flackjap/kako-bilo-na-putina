declare var document: Document | any;
declare var $: any;
declare var jQuery: any;
declare var window: Window;

export var debounce = function(func, wait, immediate?) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

export function doAjax(method, url, data, success, error) {
    var request = new XMLHttpRequest();
    request.open(method, url, true);
    // request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            success(request);
        } else {
            error(request);
        }
    };

    request.onerror = function() {
        error(request);
    };

    request.send(data);
}

let _dontPreventInCaseOf = false;

function _preventDefault(scroll) {
    scroll = scroll || window.event;

    // temp check -- should be an argument parameter
    // if ($(event.target).parents(".subscription-packets__inner, .fixed").length) {
    if (_dontPreventInCaseOf) {
        return;
    }
    if (scroll.preventDefault)
        scroll.preventDefault();
    scroll.returnValue = false;
};

function _disableScroll() {
    if (window.addEventListener) // older FF
    window.addEventListener('DOMMouseScroll', _preventDefault, false);
    window.onwheel = _preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = _preventDefault; // older browsers, IE
    window.ontouchmove  = _preventDefault; // mobile
    // document.onkeydown  = _preventDefaultForScrollKeys;
}

export function disableScroll(dontPreventInCaseOf?) {
    if (dontPreventInCaseOf) {
        _dontPreventInCaseOf = dontPreventInCaseOf;
    }
    _disableScroll();
};

function _enableScroll() {
    if (window.removeEventListener)
    window.removeEventListener('DOMMouseScroll', _preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

export function isElementInView(element, fullyInView, offsetFromTopOfTheElement) {
    let pageTop = $(window).scrollTop();
    let pageBottom = pageTop + $(window).height();
    let elementTop = $(element).offset().top + offsetFromTopOfTheElement || 0;
    let elementBottom = elementTop + $(element).height();

    if (fullyInView === true) {
        return ((pageTop < elementTop) && (pageBottom > elementBottom));
    } else {
        return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
    }
}
    
export function enableScroll() {
    _enableScroll();
};

export var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function shuffleArray(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

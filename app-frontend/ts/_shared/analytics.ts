export default function triggerGoogleAnalytics(eventName: string, formId: string) {
    // @ts-ignore: Unreachable code error
    window.dataLayer = window.dataLayer || [];
    // @ts-ignore: Unreachable code error
    window.dataLayer.push({
        'event' : eventName,
        'formId' : formId
    });
}
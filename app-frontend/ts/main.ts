import "promise-polyfill";
import "whatwg-fetch";
import "babel-polyfill";

// pages
import { initHomeUI } from "./site/home-page";
import { initSiteWideUI } from "./site-wide";
import { initBlogNewsWideUI } from "./blog-news-wide";


// partials
import { initNavigationUI } from "./_shared/navigation";

// sections (NOTE: shared acccross website)
// import ...

declare var $: any;
declare var window: Window;

let page,
    pageHasDemoSection;

/**
 * 
 *      Inits
 * 
 */

$(document).ready( _ => {

    console.log(`Document ready! Timestmap: ${Date.now()}`);
  
    page = $("body").data("page");
    pageHasDemoSection = $("body").data("page-has-demo-section");

    initNavigationUI();

    initSiteWideUI(page);

    if (page === "page-not-found") {
        // initPageNotFoundPageUI();
    }

    if (page === "home") {
        initHomeUI();
    }

    if (page === "single--blog" 
        || page === "feed--blog"
        || page === "single--news"
        || page === "feed--news"
        || page === "single--promo"
        || page === "feed--promo") {
            
            initBlogNewsWideUI();
    }

}); // document ready

import triggerGoogleAnalytics from "./_shared/analytics";
import { debounce } from "./_shared/utils";

export function initSiteWideUI(page){
    _page = page;
    $window = $(window);

    doSomething();
}

declare var $: any;
declare var window: Window;
declare var fbq: any;

/**
 * 
 *      Site-wide UI
 * 
 */

let _page: string = "",
    $window;

function doSomething() {

    console.log("Site wide init.")

}

/**
 * 
 * 
 * Responsive fixes and triggers on window "resize" event for pages NOT home
 * 
 */

function _initResponsiveResizes() {

    if (_page === "home") {
        return;
    }

    if ($window.width() < 980) {
       // 
    }

    $(window).on("resize", function(){
        if ($window.width() < 980) {
            _debounceMobileResizes();
        }
        else {
            _debounceDesktopResizes();
        }
    });

}

var _debounceMobileResizes = debounce(function () {
    // 
}, 500);

var _debounceDesktopResizes = debounce(function () {
    // 
}, 500); 



import { debounce } from "./../_shared/utils";

export function initHomeUI() {
    
    $window = $(window);

    _initResponsiveResizes();
}

declare var $: any;
declare var window: Window;

/**
 * 
 *      Home page UI
 * 
 */

let $window;

/**
 * 
 * Responsive fixes and triggers on window "resize" event
 * 
 * 
 */

function _initResponsiveResizes() { 
    if ($window.width() < 980) {
        //
    }
    $window.on("resize", function(){
        if ($window.width() < 980) {
            _debounceMobileResizes();
        }
        else {
            _debounceDesktopResizes();
        }
    })
}

var _debounceMobileResizes = debounce(function () {
    //
}, 500);

var _debounceDesktopResizes = debounce(function () {
    // 
}, 500);





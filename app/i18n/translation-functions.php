<?php

function set_lang($forced_lang) {

	global $lang;

	if ($forced_lang) {
        $lang = $forced_lang;
    }
    else {
        $lang = 'sr';
    }

}

// Simple string translation function.
function __s($token) {

	global $translation, $lang;
	
	if ( isset($translation[$token][$lang]) ) {
		return $translation[$token][$lang];
	} else {
		return $token;
	}

}

// Simple string translation function, with prefixed language string for URL parameters ("/en/").
function __ls($token, $noStartingSlash = false) {

	global $lang;

	if ($noStartingSlash) {
        return $lang . "/" . __s($token);
    }
    else {
        return "/" . $lang . "/" . __s($token);
    }
}

function language_redirect($langPart, $url) {

	// If URL has language "dir" part, then use it to set language.
	if ($langPart && in_array($langPart, ["en", "sr"])) {

		switch ($langPart) {

			case "sr": 
				set_lang("sr");
				$_SESSION["lang"] = "sr";
				break;

			case "en":
				set_lang("en");
				$_SESSION["lang"] = "en";
				break;

			default:
				set_lang("en");
		}

	}

	// Else check if there was language previously set in Session (if User visited the site already for a particular language).
	elseif (isset($_SESSION["lang"])) {
		
		$lang = get_superglobal($_SESSION, "lang");
		
		header( "Location: " . "/" . $lang . "/" . ltrim($url, "/"));
		die();

	}

	// Finally, redirect to a default language of choice, since it is not known which language User would like to get served.
	else {

		$lang = "sr";

		header( "Location: " . "/" . $lang . "/" . ltrim($url, "/") );
		die();

	}
}
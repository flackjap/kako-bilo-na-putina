<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

// Site root folder, in case it's in the subfolder
define("SITE_ROOT", "");

// Toggle multilanguage for URLs
define("PUTIN_ML", false);

// Versioning used for static assets mostly
define("WEBSITE_VER", "2.139");

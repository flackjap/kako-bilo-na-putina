<?php

function get_static_files_paths() {

    return $paths = array(

        "fontAwesome" => "/static/css--dependencies/font-awesome-4.7.0/css/font-awesome.min.css",
        "jQuery" => "/static/js--dependencies/jquery.min.js",
        "liveCSS" => "/static/css--out/style--live.css",
        "devCSS" => "/static/css--out/style--dev.css",
        "liveJS" => "/static/js--out/bundle--live.js",
        "devJS" => "/static/js--out/bundle--dev.js"

    );

}
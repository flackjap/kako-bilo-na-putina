<?php

/**
 * 
 *  
 * 
    App initializations
 */

session_start();

/**
 * 
 *  Minify html output
 */

ob_start("sanitize_output");

/**
 * 
 *  Language redirect
 */

if (PUTIN_ML) {

    // Prepare the URL.
    $url = $_SERVER["REQUEST_URI"];
    $explodedUrl = explode('/', $url);
    $firstUrlPart = $explodedUrl[1];

    // Check if we should do a redirect, since we don't redirect API routes.
    if (!in_array($firstUrlPart, ["api"])) {
        language_redirect($firstUrlPart, $url);
    }

}




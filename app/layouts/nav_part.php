<?php

function show_nav_part($partners = false) {

global $lang;

$activeLangClass = "main-nav__language-bar__active";

?>

<nav>

  <div id="main-nav" class="main-nav clearfix">

    <div class="sub-container">

      <div class="main-nav__left-part">

      <!-- @TODO - Include language links -->

      </div><!-- .main-nav__left-part -->

      <div class="main-nav__right-part float-right">

      </div><!-- .main-nav__right-part -->
      
    </div><!-- .sub-container -->

  </div><!-- #main-nav -->

</nav><!-- nav -->

<?php } ?>

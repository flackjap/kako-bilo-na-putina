<?php

function show_page_body__404() {

global $lang;

$page = "page-not-found";

?>

<body
  data-page="<?=$page?>"
  data-lang="<?=$lang?>"
  data-nojs="false"
  data-page-has-demo-section="true"
  class="<?=$page?>">

  <!-- ##################################

      HEADER

  ################################### -->

  <header>

      <?php

      show_nav_part();

      ?>

  </header>

  <main id="main-container">

    <section id="section-1--page-not-found" class="demo-section landing-section">

      <h3>Page does not exist.</h3>

    </section><!-- #section-1 -->


    <section id="section-1--page-not-found" class="demo-section landing-section full-width">
      <div class="sub-container">

          <?php show_demo_section() ?>

      </div><!-- .sub-container -->
    </section><!-- #section-1 -->


  </main><!-- end of #main-container -->


    
  <!-- ##################################

      FOOTER

  ################################### -->

  <?php
  
    show_footer();
  
  ?>

</body><!-- body -->

<?php

}

?>
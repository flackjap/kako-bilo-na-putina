<?php

function show_header($page = false) {

global $lang;

?>

<!DOCTYPE html>
<html lang="<?=($lang == 'en') ? 'sr' : 'en'?>" prefix="og: http://ogp.me/ns#">

  <head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <?= show_meta_tags($page) ?>

    <link rel="icon" type="image/png" sizes="32x32" href="/static/img/favicon.png" />

    <?= frontEndStatic__header() ?>

  </head>

<?php

}

?>

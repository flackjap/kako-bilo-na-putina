<?php

function show_page_body__home() {

global $lang;

$page = "home";

?>

<body
  data-page="<?=$page?>"
  data-lang="<?=$lang?>"
  data-nojs="false"
  data-page-has-demo-section="true"
  class="<?=$page?>">
  
  <!-- ##################################

      HEADER & MAIN NAV

  ################################### -->

  <header>

    <?php

    show_nav_part();

    ?>

  </header>

  <!-- ##################################

      MAIN CONTENT

  ################################### -->
  
  <main id="main-container" class="container">


    <!-- ##################################

        FIRST SECTION :: Something...

    ################################### -->

    <section id="section1--homepage" class="how-it-works-section full-width">

      <div class="sub-container">

        <h3>First section!</h3>

        <p>Full-width demo section.</p>

      </div>

    </section><!-- end of #section1--homepage -->


    <!-- ##################################

        SECOND SECTION :: Something something...

    ################################### -->

    <section id="section2--homepage">

      <h3>Second section!</h3>

      <p>Normal section.</p>

    </section><!-- end of #section2--homepage -->



    <!-- ##################################

        THIRD SECTION :: Something something something...

    ################################### -->

    <section id="section3--homepage">

      <?php show_demo_section() ?>

    </section><!-- end of #section3--homepage -->


  </main><!-- #main-container -->

  <!-- ##################################

      FOOTER

  ################################### -->

  <?php
  
    show_footer("home");
  
  ?>

</body><!-- body :D -->

<?php

}

?>

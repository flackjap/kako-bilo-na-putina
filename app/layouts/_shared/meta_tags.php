<?php

function show_meta_tags($page) { 
    
?>

  <?php if ($page == "home") { ?>
    <title>Website | Page title</title>
    <meta name="description" content="...">
    <meta name="keywords" content="...">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@cusername">
    <meta name="twitter:creator" content="@username">
    <meta name="twitter:title" content="...">
    <meta name="twitter:description" content="...">
    <meta name="twitter:image" content="https://...">

    <meta property="og:title" content="Website | Page title"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://..."/>
    <meta property="og:image" content="https://..."/>
    <meta property="og:site_name" content="..."/>
    <meta property="og:description" content="..."/>

  <?php } elseif ($page == "page-not-found") { ?>
    <title>Page Not Found | Website</title>
    <!-- ... -->
  <?php } ?>

  <!-- ... -->

<?php 

}

?>
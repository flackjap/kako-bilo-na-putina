<?php

function show_demo_section($page = "any") {
    
?>

<div class="demo-section demo-section__wrapper">

  <div class="demo-section__content">
    <h3>Demo "include" section!</h3>
    <p>Random content in demo section.</p>
  </div>

</div>

<?php 

} 

?>
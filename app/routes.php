<?php

ToroHook::add("404",  "PageNotFoundHandler::get");

// Add language prefixes to routes if multilanguage is active (PUTIN_ML).
global $lang;
if (PUTIN_ML) {
    $langPrefix = "/" . $lang;
    $urlPrefix = SITE_ROOT . $langPrefix;
}
else {
    $urlPrefix = SITE_ROOT;
}

// The router.
Toro::serve(array(

    "/" => "HomeHandler",
    $urlPrefix => "HomeHandler",

    // API
    SITE_ROOT . "/api/subscribe" => "SubscribeHandler",

    // EN
    $urlPrefix . "/about-us" => "AboutUsPageHandler",

    // SR
    // $urlPrefix . "/o-nama" => "AboutUsPageHandler",

));

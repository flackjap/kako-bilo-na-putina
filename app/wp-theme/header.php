<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package putinwp
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11">

<?= frontEndStatic__header() ?>

<?
  // We don't include/call `show_meta_tags` function as we do in /app/layouts/header.php, since the Wordpress handles the META and title for us
  // It's usually done through the "YoastSEO" plugin, which is also a recommended way for handling meta/og tags.
?>

<?php wp_head(); ?>

</head>


<?php

	$page = "";
	if(is_front_page()) {
		$page = "feed";
	}
	elseif (is_single()) {
		$page = "single";
	}

?>

<body data-page="<?=$page?>" data-nojs="false" data-page-has-accordions="false" data-page-has-percentages="false" <?php body_class() ?>>

  <div id="main-container" class="container site-content">

    <!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'putinwp' ); ?></a> -->
    
    <?php

      if ( is_front_page() || is_archive() ) {
        get_template_part( 'template-parts/content', 'header-home' );
      }
      if ( is_single() ) {
        get_template_part( 'template-parts/content', 'header-single' );
      }
      if ( is_404() ) {
        get_template_part( 'template-parts/content', 'header-404' );
      }

    ?>   
<?php
/**
 * Template part for displaying 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package putinwp
 */

?>

<?php if ( is_single() ) : ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class("putinwp-article-single"); ?>>
<?php else : ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php endif; ?>
	<header class="single__entry-header">
		<?php
		if ( is_single() ) :
			
			putinwp_post_author(false);
			
			putinwp_posted_on();

			// the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title blog-feed-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( has_post_thumbnail() ) {
			?><a href="<?php echo esc_url( get_permalink() ) ?>"><?php
				if ( is_single() ) {
					// the_post_thumbnail();
				}
				else {
					the_post_thumbnail('large');
				}
			?></a><?php
		}

		if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php 
					if ( !is_single() ) {
						putinwp_posted_on();
					} 
				?>
			</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			if (is_front_page() || is_archive()) {

				$excerpt = get_the_excerpt();
				if ($excerpt!='') echo "<p>" . $excerpt . "</p>";
				else echo "<p>" . wp_trim_words( get_the_content(), 25 ) . "</p>";

			}
			else {

				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'putinwp' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'putinwp' ),
					'after'  => '</div>',
				) );
			}
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php putinwp_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

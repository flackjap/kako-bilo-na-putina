<?php
/**
 * Template part for displaying 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package putinwp
 */

?>

<?php 
    the_post(); 
    $featuredImgURL = get_the_post_thumbnail_url($post, 'large');
?>

<div id="hero-header" class="hero-header hero-header-single full-width" data-featured-img-url="<?=$featuredImgURL?>">
  <div class="hero-header__overlay"></div><!-- .hero-header__overlay -->
  <div class="sub-container main-nav__container">

    <?=show_nav_part()?>

    <div class="hero-header__content">

      <div class="hero-header__texts">

        <?php

            // date
            // putinwp_posted_on();
            
            // title
            the_title( '<h1 class="hero-header__h1 entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );

            // author
            // putinwp_post_author(false);

            // restore back the single post to the loop so it can be used by single.php
            rewind_posts();

        ?>
        
      </div><!-- .hero-header__texts -->

    </div><!-- .hero-header__content -->

  </div><!-- .sub-container -->

</div><!-- .hero-header -->
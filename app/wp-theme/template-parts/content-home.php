<?php
/**
 * Template part for displaying 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package putinwp
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("feed__article-box"); ?>>
	<header class="feed__article-box__entry-header">
		<?php

            /**
                Thumbnail
            */
            if ( has_post_thumbnail() ) {
                ?><a href="<?php echo esc_url( get_permalink() ) ?>"><?php
                    if ( is_single() ) {
                        the_post_thumbnail(array(
                            'class' => 'feed__article-box__img'
                        ));
                    }
                    else {
                        the_post_thumbnail('large', array(
                            'class' => 'feed__article-box__img'
                        ));
                    }
                ?></a><?php
            }
?>
	</header><!-- .entry-header -->

	<div class="feed__article-box__entry-content">
		<?php

            /**
                Date
            */
            if ( 'post' === get_post_type() ) : ?>
                <div class="entry-meta">
                    <?php putinwp_posted_on(); ?>
                </div><!-- .entry-meta -->
                <?php
            endif;
            
            /**
                Title
            */
            the_title( '<h2 class="feed__article-box__entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

            /**
                Excerpt
            */
            $excerpt = get_the_excerpt();
            if ($excerpt!='') echo "<p class=\"feed__article-box__entry-p\">" . $excerpt . "</p>";
            else echo "<p class=\"feed__article-box__entry-p\">" . wp_trim_words( get_the_content(), 25 ) . "</p>";
		?>
	</div><!-- .entry-content -->

	<!--<footer class="entry-footer">
		<?php putinwp_entry_footer(); ?>
	</footer>-->
</article><!-- #post-## -->

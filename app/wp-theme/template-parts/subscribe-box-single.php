<?php
/**
 * Template part for displaying
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package putinwp
 */

?>

<article class="feed__article-box home-subscribe-box home-post-box__plus-920-gone">

	<header class="entry-header feed__article-box__entry-header">
		<div class="blue-overlay"></div>
		<div class="feed__article-box__abs-container absolute text-center">
			<!--<img class="blue-logo relative" src="/static/img/new/blue-logo.png">-->
			<h2 class="feed__article-box__title relative">Prijavite se za najnovije vesti</h2>
		</div>
		<img width="640" height="336" src="static/img--new/sidebar-widget-image.png" class="feed__article-box__img attachment-large size-large wp-post-image" sizes="(max-width: 640px) 100vw, 640px">

	</header><!-- .entry-header -->

	<div class="feed__article-box__entry-content entry-content">
		<p class="feed__article-box__entry-p home-subscribe-p">We occasionally posts news and like to share them over our newsletter!</p>
		<div class="text-center feed__article-box__bottom-part">
			<input class="feed__article-box__input block center-by-margs" id="mail-input-1" placeholder="Vaša e-mail adresa">
			<button class="feed__article-box__btn btn" id="subscribe-btn-1">Prijavljujem se</button>
			<fieldset class="feed__article-box__checkbox-container">
				<input type="checkbox" name="partner-sub" id="partner-sub-1">
				<span> Želim da primam i dodatne informacije o servisu</span>
			</fieldset>

		</div>
		<div class="subscription-message">
			<div class="bubblingG notification-spinner" id="notification-spinner-1">
				<span id="bubblingG_1">
				</span>
				<span id="bubblingG_2">
				</span>
				<span id="bubblingG_3">
				</span>
			</div>
			<span class="subscription-message__text" id="subscription-message__text-1"></span>
		</div>
	</div><!-- .entry-content -->

</article><!-- #post-## -->

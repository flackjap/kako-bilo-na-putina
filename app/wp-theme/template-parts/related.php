<?php
/**
 * Template part for displaying related posts below the article.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package putinwp
 */

?>

<div id="post-nav">

    <?php // @TODO - i18n ... ?>

    <h2 class="widget-title">Još objava</h2>
    <br class="hidden-sm" />
    <?php $prevPost = get_previous_post(true);
        if($prevPost) {
            $args = array(
                'posts_per_page' => 1,
                'include' => $prevPost->ID
            );
            $prevPost = get_posts($args);
            foreach ($prevPost as $post) {
                setup_postdata($post);
    ?>
        <div class="post-previous">
            <a href="<?php the_permalink(); ?>">
                <div class="related-post-img-wrapper">
                    <?php the_post_thumbnail('medium'); ?>
                </div>
            </a>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <small><?php the_date('F j, Y'); ?></small>
        </div>
    <?php
                wp_reset_postdata();
            } //end foreach
        } // end if

        $nextPost = get_next_post(true);
        if($nextPost) {
            $args = array(
                'posts_per_page' => 1,
                'include' => $nextPost->ID
            );
            $nextPost = get_posts($args);
            foreach ($nextPost as $post) {
                setup_postdata($post);
    ?>
        <div class="post-next">
            <a href="<?php the_permalink(); ?>">
                <div class="related-post-img-wrapper">
                    <?php the_post_thumbnail('medium'); ?></a>
                </div>
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <small><?php the_date('F j, Y'); ?></small>
        </div>
    <?php
                wp_reset_postdata();
            } //end foreach
        } // end if
        else {
            $first = new WP_Query('posts_per_page=1&order=ASC');
            $first->the_post(); ?>
            <div class="post-next">
                <a href="<?php the_permalink(); ?>">
                    <div class="related-post-img-wrapper">
                        <?php the_post_thumbnail('medium'); ?></a>
                    </div>
                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <small><?php the_date('F j, Y'); ?></small>
            </div>
    <?php
            wp_reset_postdata();
        } // end else
    ?>
</div>

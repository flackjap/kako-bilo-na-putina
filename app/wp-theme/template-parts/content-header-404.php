<?php
/**
 * Template part for displaying 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package putinwp
 */

?>

<div class="hero-header page-not-found-hero-header landing-hero-header full-width">
    <div class="hero-header__overlay"></div><!-- .hero-header__overlay -->
    <div class="sub-container main-nav__container">
      <?=show_nav_part()?>
      <div class="hero-header__content center-x-and-y text-center">

        <div class="hero-header__texts">

          <h1 class="hero-header__h1">
            <?=__('Greška 404')?>
          </h1>

          <p class="hero-header__p"><?=__('Tražena stranica ne postoji :(')?></p>
          
        </div><!-- .hero-header__texts -->

      </div><!-- .hero-header__content -->

    </div><!-- .sub-container -->
    
</div><!-- .hero-header -->
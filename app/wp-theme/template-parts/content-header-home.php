<?php
/**
 * Template part for displaying 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package putinwp
 */

?>

<?php 
    the_post(); 
    $featuredImgURL = get_the_post_thumbnail_url($post, 'large');
?>

<div id="hero-header" class="hero-header hero-header-blog-feed full-width" data-featured-img-url="<?=$featuredImgURL?>">
  
  <div class="hero-header__overlay"></div><!-- .hero-header__overlay -->
  <div class="sub-container main-nav__container">

  <?=show_nav_part()?>

    <div class="hero-header__content">

      <div class="hero-header__texts">

        <?php
        
            // date
            putinwp_posted_on();
            
            // title
            the_title( '<h2 class="hero-header__h1 entry-title"><a class="hero-header__a" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

            // excerpt
            $excerpt = get_the_excerpt();
            if ($excerpt!='') echo "<p class=\"hero-header__p\">" . $excerpt . "</p>";
            else echo "<p class=\"hero-header__p\">" . wp_trim_words( get_the_content(), 25 ) . "</p>";

            // author
            putinwp_post_author(false);

        ?>
        
      </div><!-- .hero-header__texts -->

    </div><!-- .hero-header__content -->

  </div><!-- .sub-container -->
  
</div><!-- .hero-header -->
<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package putinwp
 */

get_header();
// the_breadcrumb();

	?>

	<section id="primary" class="content-area single-content-area" role="main">

	<?php

		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

			// the_post_navigation();

			// get_template_part( 'template-parts/subscribe-box-single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;


		endwhile; // End of the loop.

		?>

	</section><!-- #primary -->

<?php

get_sidebar();

get_footer();

<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package putinwp
 */

get_header(); 
global $query_string;
query_posts('posts_per_page=-1');

?>

	<section id="primary" class="content-area feed" role="main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && is_front_page() ) : ?>

			<?php

				// Check the number of posts and quit displaying the feed if we have only one post that's already displayed in the hero header.
				global $wp_query; 
				if ($wp_query->found_posts <= 1) {

					// do nothing.
					
				}
				else {

					$countForSubscribeBox = 0;

					/* Start the Loop */
					while ( have_posts() ) : the_post();


						$countForSubscribeBox++;

						if ($countForSubscribeBox == 6) {
							get_template_part( 'template-parts/subscribe-box-home' );
						}

						/*
						* Include the Post-Format-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Format name) and that will be used instead.
						*/
						get_template_part( 'template-parts/content', 'home' );


					endwhile;

				}
			
			else :
				
				get_template_part( 'template-parts/content', get_post_format() );

			endif;

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</section><!-- #primary -->

<?php

// get_sidebar();
get_footer();

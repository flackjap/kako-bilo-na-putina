<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package putinwp
 */


/**
	Display post date "posted on"
*/

if ( ! function_exists( 'putinwp_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post date created
 */
function putinwp_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}
	else {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$calendarFaIcon = '<i class="feed__calendar-icon fa fa-calendar-o" aria-hidden="true"></i>';
	if (is_single()) {
		$calendarFaIcon = '<i class="single__calendar-icon fa fa-calendar-o" aria-hidden="true"></i>';
	}

	$posted_on = sprintf(
		// esc_html_x( 'Posted on %s', 'post date', 'putinwp' ),
		esc_html_x( '%s', 'post date', 'putinwp' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $calendarFaIcon . $time_string . '</a>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

}
endif;

/**
	Display post author
*/
if ( ! function_exists( 'putinwp_post_author' ) ) :
/**
 * Prints HTML with meta information (name, last name, and profile photo) for the current post author.
 */
function putinwp_post_author($postType) {


    // @TODO - Temporarily commented out. Hardcoded avatars for different authors. Should be connected to user profiles uploaded from within the WordPress.
//	if ($postType) {
//		if (get_the_author_meta( 'ID' ) == 1) {
//			$authorPhoto = '<img id="avatartest2" class="single__author-photo" src="/static/img/avatar.png">';
//		}
//		elseif (get_the_author_meta( 'ID' ) == 3) {
//			$authorPhoto = '<img id="avatartest2" class="single__author-photo" src="/static/img/new/autor-nenad-stamenkovic.jpg">';
//			// $authorPhoto2 = get_avatar( get_the_author_meta( 'ID' ));
//		}
//		else {
//			$authorPhoto = '<img id="avatartest2" class="single__author-photo" src="/static/img/avatar.png">';
//		}
//	}
//	else {
//		$authorPhoto = '';
//	}

    $authorPhoto = '<img id="avatartest2" class="single__author-photo" src="/static/img/avatar.png">';

	$authorFaIcon = "";
	if (is_single()) {
		$authorFaIcon = '<i class="single__author-icon fa fa-pencil-square-o" aria-hidden="true"></i>';
	}

	echo sprintf(
		// esc_html_x( 'by %s', 'post author', 'putinwp' ),
		esc_html_x( '%s', 'post author', 'putinwp' ),
		$authorPhoto . $authorFaIcon . '<span class="author vcard">Autor: <a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" style="pointer-events: none;">' . esc_html( get_the_author() ) . '</a></span>'
	);
	// echo '<div id="avatartest" style="display: none;">' . get_the_author_meta( 'ID' ) . '</div>';
}
endif;

/**
	Display post tags and categories
*/

if ( ! function_exists( 'putinwp_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function putinwp_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'putinwp' ) );
		if ( $categories_list && putinwp_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Kategorija: %1$s', 'putinwp' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'putinwp' ) );
		if ( $tags_list ) {
			// printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'putinwp' ) . '</span>', $tags_list ); // WPCS: XSS OK.
            // @TODO - Should use the i18n functions.
			printf( '<span class="tags-links">' . esc_html__( 'Tagovi: %1$s', 'putinwp' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}
}
endif;

/**
	Check if the blog post has more than 1 category
*/
/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function putinwp_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'putinwp_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'putinwp_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so putinwp_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so putinwp_categorized_blog should return false.
		return false;
	}
}

/**
	Flush out the transients used in putinwp_categorized_blog.
 */
 
function putinwp_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'putinwp_categories' );
}
add_action( 'edit_category', 'putinwp_category_transient_flusher' );
add_action( 'save_post',     'putinwp_category_transient_flusher' );

/**
	Display breadcrumbs
*/

function the_breadcrumb() {

    echo '<ul id="crumbs">';

	if (!is_home()) {
		echo '<li><a href="';
		echo get_option('home');
		echo '">';
		echo 'Blog';
		echo "</a></li>";
		if (is_category() || is_single()) {
			echo '<li>';
			the_category(' </li><li> ');
			echo '</li>';
			// if (is_single()) {
			// 	echo "</li><li>";
			// 	the_title();
			// 	echo '</li>';
			// }
		} elseif (is_page()) {
			echo '<li>';
			echo the_title();
			echo '</li>';
		} elseif (is_tag()) {
			echo '<li>';
			single_tag_title();
			echo '</li>';
		}
	}
	elseif (is_tag()) {single_tag_title();}
	elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
	elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
	elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
	elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
	elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
	elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}

	echo '</ul>';
}

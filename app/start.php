<?php

/**
 * 
 * 
    Utilities first
 */


include "utils/functions.php";

 /**
 * 
 * 
    i18n + Translations
*/

include 'i18n/translations.php';
include 'i18n/translation-functions.php';


/**
 * 
 * 
    Configs & Dependencies
*/

include "config/config.php";
include "config/static_files_paths.php";
include "config/env.php";
include "config/dependencies.php";
include "config/inits.php";


/**
 * 
 * 
    App files
*/

// Actions & API handlers
include "controllers/SubscribeHandler.php";
include "actions/frontEndStatic--header.php";
include "actions/frontEndStatic--footer.php";

// Page layouts
include "layouts/header.php";
include "layouts/nav_part.php";
include "layouts/pageBody_home.php";
include "layouts/pageBody_404.php";
include "layouts/footer.php";

// shared layouts
include "layouts/_shared/demo_section.php";
include "layouts/_shared/meta_tags.php";

// Page handlers
include "controllers/HomeHandler.php";
include "controllers/404Handler.php";


/**
 * 
 *     
    Router

*/

include "routes.php";
<?php

function site_root() {
    if (SITE_ROOT !== "") {
        echo "/" . SITE_ROOT . "/";
    }
    else {
        echo "";
    }
}

function get_site_root() {
    if (SITE_ROOT !== "") {
        return "/" . SITE_ROOT . "/";
    }
    else {
        return "/";
    }
}

function static_root_url() {
    echo get_site_root() . "static/";
}

/**
 *
 * Securely obtain data from PHP's superglobals.
 *
 * @param $array - $_POST, $_GET, $_SESSION, etc.
 * @param string $index - array string index key (e.g. $_POST["index"]).
 * @param null|string $default - default value to return if no other is present.
 * @return null|string - will either return a string, default value above, or null.
 */
function get_superglobal($array, $index, $default = null) {
    if (isset($array[$index]) && strlen($value = trim($array[$index])) > 0) {
         return get_magic_quotes_gpc() ?  stripslashes($value) : $value;
    } else {
         return $default;
    }
}

function httpPost($url, $data) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

function sanitize_output($buffer) {
    $search = array(
        '/\>[^\S ]+/s', // strip whitespaces after tags, except space
        '/[^\S ]+\</s', // strip whitespaces before tags, except space
        '/(\s)+/s'  // shorten multiple whitespace sequences
        );
    $replace = array(
        '>',
        '<',
        '\\1'
        );

    $blocks = preg_split('/(<\/?pre[^>]*>)/', $buffer, null, PREG_SPLIT_DELIM_CAPTURE);
    $buffer = '';
    foreach($blocks as $i => $block)
    {
      if($i % 4 == 2)
        $buffer .= $block; // break out <pre>...</pre> with \n's
      else
        $buffer .= preg_replace($search, $replace, $block);
    }

    return $buffer;
}

function getUrlWrappedWithHttp($url) {
    return (!empty($_SERVER["HTTPS"]) ? "https" : "http") . "://" . $url . "/";
}

function formatPriceToRSDformat($price) {
    return number_format( floatval($price) , 2 , "," ,  "." );
}

?>

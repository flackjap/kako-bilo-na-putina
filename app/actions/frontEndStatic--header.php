<?php

function frontEndStatic__header() {

    $static = "";

    $files = get_static_files_paths();

    if (ENV == "dev") {

        $static .= '<link rel="stylesheet" id="font-awesome-styles" 
                        href="'
                        . $files["fontAwesome"]
                        . '?ver=' . rand() .
                    '" type="text/css" media="all" />';

	    $static .= '<link rel="stylesheet" id="styles-bundle" 
	                    href="'
                        . $files["devCSS"]
                        . '?ver=' . rand() .
                    '" type="text/css" media="all" />';

	    $static .= '<script type="text/javascript" src="' . $files["devJS"] . '?ver=' . rand() . '"></script>';

    } elseif (ENV == "live") {

        $static .= '<link rel="stylesheet" id="styles-bundle" 
                        href="/min/index.php?f='
                        . $files["liveCSS"] . ','
                        . $files["fontAwesome"]
                        . '&ver=' . WEBSITE_VER .
                    '" type="text/css" media="all" />';

        $static .= '<script type="text/javascript" 
                        src="/min/index.php?f='
                        . $files["jQuery"] . ','
                        . $files["liveJS"]
                        . '&ver=' . WEBSITE_VER .
                    '"></script>';

        $static .= '<script>
          (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');
    
          ga(\'create\', \'<?=__s(\'GA-ID\')?>\', \'auto\');
          ga(\'send\', \'pageview\');
        </script>';

    }

    return $static;

}
'use strict';

var gulp = require('gulp');

/**
 * 
 *      TypeScript
 * 
 */

var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var tsify = require('tsify');
var gutil = require("gulp-util");
var sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var log = require('fancy-log');

var watchedBrowserify = watchify(
    browserify({
        basedir: '.',
        debug: true,
        entries: ['app-frontend/ts/main.ts'],
        cache: {},
        packageCache: {}
    })
    .plugin(tsify)
    .transform('babelify', {
        // presets: ['es2015', 'stage-0'],
        presets: ['es2015'],
        extensions: ['.ts']
        // plugins: ["transform-async-to-generator", "@babel/transform-runtime"]
    }),
    {
        poll: true
    }
);

function logError(err) {
    gutil.log(err.message);
    this.emit('end');
}

function bundleDev() {
    return watchedBrowserify
        .bundle()
        .pipe(source('bundle--dev.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('static/js--out'))
        .on('end', function(){ log('Dev bundle done!'); });
};

function bundleLive() {
    return watchedBrowserify
        .bundle()
        .pipe(source('bundle--live.js'))
        .pipe(buffer())
        .pipe(gulp.dest('static/js--out'))
        .on('end', function(){ log('Live bundle done!'); });
}


function tempBundleDev() {
    return browserify({
            basedir: '.',
            debug: true,
            entries: ['app-frontend/ts/main.ts'],
            cache: {},
            packageCache: {}
        })
        .plugin(tsify)
        .transform('babelify', {
            presets: ['es2015'],
            extensions: ['.ts']
        })
        .bundle()
        .pipe(source('bundle--dev.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('static/js--out/'));
};

function tempBundleLive() {
    return browserify({
            basedir: '.',
            debug: true,
            entries: ['app-frontend/ts/main.ts'],
            cache: {},
            packageCache: {}
        })
        .plugin(tsify)
        .transform('babelify', {
            presets: ['es2015'],
            extensions: ['.ts']
        })
        .bundle()
        .pipe(source('bundle--live.js'))
        .pipe(gulp.dest('static/js--out/'));
};

gulp.task('dev', tempBundleDev);
gulp.task('live', tempBundleLive);

// watchedBrowserify.on('update', bundleDev);
// watchedBrowserify.on('update', bundleLive);
// watchedBrowserify.on('log', gutil.log);

/**
 * 
 *      SASS / SCSS
 * 
 *      -- we'll be using the node-sass directly with watch for now
 *      -- and running postcss (autoprefixer) directly for production
 * 
 */

// var sass = require('gulp-sass');

// gulp.task('sass', function () {
//     return gulp.src('./app-frontend/scss/main.scss')
//       .pipe(sass().on('error', sass.logError))
//       .pipe(gulp.dest('./static/css--out'));
// });
   
// gulp.task('sass:watch', function () {
//     gulp.watch('./app-frontend/scss/main.scss', ['sass']);
// });
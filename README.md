# Kako bilo na putina?

# ... is a scaffolding tool for creating websites with almost static pages that share styles and javascript with a blog engine (currently Wordpress only), that runs as a separate instance in its own folder and a base route path.

The idea was to have blazingly fast landing pages, while still having a full blog engine running aside. To get the maximum page loading speed, you'd either have to get rid of the CMS like WordPress (WP) and run your blog in some form of a static CMS, which has its own downsides, or you would put your blog on Medium, or you would even run your blog on WP, but with a totally separated theme, with its own config, env, translation files and similar shared php files, and of course a theme that has its own static files (styles and javascript specifically), which would make updating the website a bit tedious and website visitors would download static files twice.

"Kako bilo na putina" is simply a scaffolding for the previously explained, the already wired static + blog thing, but it's also a very simple structure for developing landing pages without running a fully fledged framework (with or without database), which also has impact on page loading speed and adds a level of complexity to swiftly changing and creating simple web landing pages. 


Setup:

```
bash setup.sh
composer install
npm install

Open yourdomain.com/blog and complete the Wordpress installation.
```